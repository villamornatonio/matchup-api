<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$router->post('auth/token', 'Api\AuthController@authenticate');
$router->post('auth/personalToken', 'Api\AuthController@authenticatePersonalToken');
$router->post('auth/refresh', 'Api\AuthController@refreshToken');
$router->get('user', 'Api\AuthController@getUser')->middleware('auth:api');

$router->group(['prefix' => 'v1/','middleware' => 'auth:api'], function ($router) {
    $router->group(['prefix' => 'users'], function ($router) {
        $router->get('/', 'Api\UsersController@index');
        $router->post('/create', 'Api\UsersController@store');
    });

    $router->group(['prefix' => 'notifications'], function ($router) {
        $router->get('/', 'Api\NotificationController@index');
        $router->put('/mark-all-as-read', 'Api\NotificationController@markAllAsRead');
        $router->put('{id}/mark-as-read', 'Api\NotificationController@markAsRead');
    });


    $router->group(['prefix' => 'superadmin','middleware' => 'can:api.superadmin.only'], function ($router) {
        $router->group(['prefix' => 'teachers'], function ($router) {
            $router->get('/', 'Api\Admin\TeacherController@index');

        });

    });

    $router->group(['prefix' => 'admin','middleware' => 'can:api.admin.only'], function ($router) {
        $router->group(['prefix' => 'teachers'], function ($router) {
            $router->get('/', 'Api\Admin\TeacherController@index');
        });

        $router->group(['prefix' => 'students'], function ($router) {
            $router->get('/', 'Api\Admin\StudentController@index');
        });

        $router->group(['prefix' => 'announcements'], function ($router) {
            $router->get('/', 'Api\Admin\AnnouncementController@index');
            $router->get('{id}/edit', 'Api\Admin\AnnouncementController@edit');
            $router->post('/create', 'Api\Admin\AnnouncementController@store');
            $router->put('{id}/update', 'Api\Admin\AnnouncementController@update');
            $router->delete('{id}/delete', 'Api\Admin\AnnouncementController@delete');
            $router->delete('/delete-all', 'Api\Admin\AnnouncementController@deleteAll');
        });

    });

    $router->group(['prefix' => 'teacher','middleware' => 'can:api.teacher.only'], function ($router) {
        $router->group(['prefix' => 'teachers'], function ($router) {
            $router->get('/', 'Api\Admin\TeacherController@index');
        });

    });

    $router->group(['prefix' => 'guardian','middleware' => 'can:api.guardian.student.only'], function ($router) {
        $router->group(['prefix' => 'teachers'], function ($router) {
            $router->get('/', 'Api\Admin\TeacherController@index');
        });

    });

    $router->group(['prefix' => 'student','middleware' => 'can:api.guardian.student.only'], function ($router) {
        $router->group(['prefix' => 'teachers'], function ($router) {
            $router->get('/', 'Api\Admin\TeacherController@index');
        });

    });
});
