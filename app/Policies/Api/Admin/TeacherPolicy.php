<?php

namespace App\Policies\Api\Admin;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class TeacherPolicy
 * @package App\Policies\Api\Admin
 */
class TeacherPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function index(User $user)
    {

        return $user->role->name === 'admin';
    }

}
