<?php

namespace App\Policies;


use App\Models\Announcement;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class AnnouncementPolicy
 * @package App\Policies
 */
class AnnouncementPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Announcement $announcement
     * @return bool
     */
    public function owned(User $user, Announcement $announcement)
    {
        return $announcement->created_by === $user->id;

    }

}
