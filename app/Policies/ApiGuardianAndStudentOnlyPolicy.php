<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ApiGuardianAndStudentOnlyPolicy
 * @package App\Policies
 */
class ApiGuardianAndStudentOnlyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function only(User $user)
    {
        return $user->role->role->name === 'guardian' || $user->role->role->name === 'student';

    }
}
