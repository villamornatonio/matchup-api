<?php

namespace App\Policies;

use App\Models\Notification;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NotificationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function owned(User $user, Notification $notification)
    {
        return $notification->user_id === $user->id;

    }
}
