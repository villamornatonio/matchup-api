<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ApiTeacherOnlyPolicy
 * @package App\Policies
 */
class ApiTeacherOnlyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function only(User $user)
    {
        return $user->role->role->name === 'teacher';

    }
}
