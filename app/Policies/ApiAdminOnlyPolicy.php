<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class ApiAdminOnlyPolicy
 * @package App\Policies
 */
class ApiAdminOnlyPolicy
{
    use HandlesAuthorization;

    /**
     * ApiAdminOnlyPolicy constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param User $user
     * @return bool
     */
    public function only(User $user)
    {
        return $user->role->role->name === 'admin';

    }
}
