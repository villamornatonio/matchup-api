<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class School
 * @package App\Models
 */
class School extends Model
{
    /**
     * @var string
     */
    protected $table = 'schools';
    /**
     * @var array
     */
    protected $fillable = ['short_name', 'long_name', 'address','avatar_url'];
}
