<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    /**
     * @var string
     */
    protected $table = 'user_roles';
    /**
     * @var array
     */
    protected $fillable = ['role_id', 'user_id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('defaultsRelationships', function ($builder) {
            $builder->with(['role']);
        });

    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

}
