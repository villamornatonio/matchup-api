<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Announcement
 * @package App\Models
 */
class Announcement extends Model
{
    /**
     * @var string
     */
    protected $table = 'announcements';
    /**
     * @var array
     */
    protected $fillable = ['title', 'content', 'date', 'school_id', 'created_by'];


    /**
     *
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('defaultsRelationships', function ($builder) {
            $builder->with(['school', 'createdBy']);
        });

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function school()
    {
        return $this->hasOne('App\Models\School', 'id', 'school_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function createdBy()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }

}
