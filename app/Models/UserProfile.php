<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    /**
     * @var string
     */
    protected $table = 'user_profiles';
    /**
     * @var array
     */

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('defaultsRelationships', function ($builder) {
            $builder->with(['school']);
        });

    }
    protected $fillable = ['user_id', 'school_id','rfid','avatar'];


    public function school()
    {
        return $this->hasOne(School::class, 'id', 'school_id');
    }
}

