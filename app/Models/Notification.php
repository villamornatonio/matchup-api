<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Notification
 * @package App\Models
 */
class Notification extends Model
{
    /**
     * @var string
     */
    protected $table = 'notifications';
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'title', 'content', 'icon', 'is_read'];
}
