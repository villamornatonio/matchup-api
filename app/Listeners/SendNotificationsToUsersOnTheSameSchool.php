<?php

namespace App\Listeners;

use App\Events\AnnouncementWasCreated;
use App\Models\Notification;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotificationsToUsersOnTheSameSchool implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  AnnouncementWasCreated  $event
     * @return void
     */
    public function handle(AnnouncementWasCreated $event)
    {
        $user = User::find($event->announcement->created_by);
        $users = User::where('school_id', $user->school_id)->whereHas('userProfile', function($query) use ($user){
            $query->where('school_id', $user->userProfile->school_id);
        })->get();

        foreach ($users as $user)
        {
            $data = [
                'title' => 'NEW ANNOUNCEMENT',
                'content' => $event->announcement->content,
                'icon' => 'fa-envelope',
                'user_id' => $user->id
            ];
            Notification::create($data);

        }
    }
}
