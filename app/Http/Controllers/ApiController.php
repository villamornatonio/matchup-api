<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    protected $statusCode = 200;

    public function __construct()
    {

    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;

    }


    /**
     * @param $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @param string $message
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function respond($message = '', $data = [])
    {
        $response['message'] = $message;
        $response['status'] = $this->getStatusCode();
        if(!empty($data)){
            $response['data'] = $data;
        }
        return response()->json($response, $this->getStatusCode());
    }


    public function responseWithError($message = '')
    {
        $this->statusCode = 400;

        return $this->respond($message);

    }

    public function respondDatatables($message = '', $data = [])
    {
        $response = $data;
        $response['message'] = $message;
        $response['status'] = $this->getStatusCode();

        return response()->json($response, $this->getStatusCode());
    }

    public function respondPaginated($message = '', $data = [])
    {
        $response = $data;
        $response['message'] = $message;
        $response['status'] = $this->getStatusCode();

        return response()->json($response, $this->getStatusCode());
    }


}
