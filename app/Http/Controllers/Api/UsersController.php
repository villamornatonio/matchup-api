<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Repositories\Api\UserRepository;
use App\Transformers\UsersTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


/**
 * Class UsersController
 * @package App\Http\Controllers\Api
 */
class UsersController extends ApiController
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UsersController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $data = $this->userRepository->all()->transformedCollection();

        return $this->respond('Success', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $parameters = $request->only('first_name', 'last_name', 'email', 'password');

        $data = [
            'first_name' => $parameters['first_name'],
            'last_name' => $parameters['last_name'],
            'email' => $parameters['email'],
            'password' => \Hash::make($parameters['password']),
        ];

        if (!$this->userRepository->create($data)) {
            return $this->responseWithError('Failed');
        }

        return $this->respond('Success', $data);

    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, Request $request)
    {
        $data = $this->userRepository->show($id)->transformed();

        return $this->respond('Success', $data);
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $parameters = $request->only('first_name', 'last_name', 'email', 'password');

        $data = [
            'first_name' => $parameters['first_name'],
            'last_name' => $parameters['last_name'],
        ];

        if (!$this->userRepository->update($id, $data)) {
            return $this->responseWithError('Failed');
        }

        return $this->respond('Success', $data);

    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, Request $request)
    {
        if (!$this->userRepository->delete($id)) {
            return $this->responseWithError('Failed');
        }

        return $this->respond('Success');
    }
}
