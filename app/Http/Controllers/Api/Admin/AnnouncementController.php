<?php

namespace App\Http\Controllers\Api\Admin;

use App\Events\AnnouncementWasCreated;
use App\Http\Controllers\ApiController;
use App\Http\Requests\StoreAnnouncementRequest;
use App\Repositories\Api\AnnouncementRepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class AnnouncementController
 * @package App\Http\Controllers\Api\Admin
 */
class AnnouncementController extends ApiController
{
    /**
     * @var AnnouncementRepositoryInterface
     */
    protected $announcementRepository;

    /**
     * AnnouncementController constructor.
     * @param AnnouncementRepositoryInterface $announcementRepository
     */
    public function __construct(AnnouncementRepositoryInterface $announcementRepository)
    {
        $this->announcementRepository = $announcementRepository;

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $data = $this->announcementRepository->ownSchool($request);

        return $this->respondPaginated('Success', $data);

    }

    /**
     * @param StoreAnnouncementRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreAnnouncementRequest $request)
    {
        $parameters = $request->only(['title', 'content', 'date']);
        $dataParameters = [
            'title' => $parameters['title'],
            'content' => $parameters['content'],
            'date' => $parameters['date'],
            'school_id' => $request->user()->school_id,
            'created_by' => $request->user()->id,
        ];
        $data = $this->announcementRepository->create($dataParameters);

        event(new AnnouncementWasCreated($data));

        return $this->respond('Success', $data);

    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id, Request $request)
    {
        $data = $this->announcementRepository->show($id);
        $this->authorize('api.announcement.owned', $data);

        return $this->respond('Success', $data);

    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $data = $this->announcementRepository->show($id);
        $this->authorize('api.announcement.owned', $data);

        $parameters = $request->only(['title', 'content', 'date']);
        $dataParameters = [
            'title' => $parameters['title'],
            'content' => $parameters['content'],
            'date' => $parameters['date']
        ];
        $data = $this->announcementRepository->update($dataParameters, $id);

        return $this->respond('Success', $data);

    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id, Request $request)
    {
        $data = $this->announcementRepository->show($id);
        $this->authorize('api.announcement.owned', $data);
        $this->announcementRepository->delete($id);

        return $this->respond('Success', $data);


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAll(Request $request)
    {
        $data = $this->announcementRepository->deleteAllOwned($request);

        return $this->respond('Success', $data);
    }
}
