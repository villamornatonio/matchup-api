<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\ApiController;
use App\Http\Resources\UserResource;
use App\Repositories\Api\UserRepository;
use App\Transformers\UsersTransformer;
use App\User;
use Illuminate\Http\Request;

/**
 * Class TeacherController
 * @package App\Http\Controllers\Api\Admin
 */
class TeacherController extends ApiController
{
    /**
     * TeacherController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

//        $data = $this->userRepository->all()->transformedCollection();

//        $query = User::orderBy($request->column, $request->order);
        $userTransformer = new UsersTransformer();
//
        $query = User::query();
////        $query = User::all();
        $data = $query->paginate($request->per_page)->toArray();

        $transformedData = $userTransformer->transformCollection($data['data']);
//        unset($rawData['data']);
        $data['data'] = $transformedData;
        $data['message'] = 'Success';
        $data['code'] = 200;

//
        return response($data);
//        $data = $query->paginate($request->per_page);

//        return $data;



//        $data['data'] = $userTransformer->transformCollection($query->to());
//        return UsersResource::collection($users);

        return $this->respond('Success', $rawData);

    }

}
