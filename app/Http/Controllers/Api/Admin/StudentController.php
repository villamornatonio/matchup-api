<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\ApiController;
use App\Repositories\Api\StudentRepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class StudentController
 * @package App\Http\Controllers\Api\Admin
 */
class StudentController extends ApiController
{
    /**
     * @var StudentRepositoryInterface
     */
    protected $studentRepository;

    /**
     * StudentController constructor.
     * @param StudentRepositoryInterface $studentRepository
     */
    public function __construct(StudentRepositoryInterface $studentRepository)
    {
        $this->studentRepository = $studentRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

        $data = $this->studentRepository->paginatedStudents($request);

        return $this->respondPaginated('Success', $data);

    }
}
