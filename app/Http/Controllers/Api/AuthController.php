<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Transformers\UsersTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;


/**
 * Class AuthController
 * @package App\Http\Controllers\Api
 */
class AuthController extends ApiController
{

    /**
     * @var
     */
    private $client;


    /**
     * @var UsersTransformer
     */
    protected $usersTransformer;

    /**
     * @param UsersTransformer $usersTransformer
     */
    public function __construct(UsersTransformer $usersTransformer)
    {
        $this->client = DB::table('oauth_clients')->where('id', 2)->first();
        $this->usersTransformer = $usersTransformer;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function authenticate(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|email',
            'password' => 'required',
        ]);
        $request->request->add([
            'username' => $request->username,
            'password' => $request->password,
            'grant_type' => 'password',
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'scope' => '*'
        ]);

        $proxy = Request::create(
            'oauth/token',
            'POST'
        );

        return Route::dispatch($proxy);
    }


    /**
     * @param Request $request
     * @return mixed
     */
    protected function refreshToken(Request $request)
    {
        $request->request->add([
            'grant_type' => 'refresh_token',
            'refresh_token' => $request->refresh_token,
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
        ]);

        $proxy = Request::create(
            '/oauth/token',
            'POST'
        );

        return Route::dispatch($proxy);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser(Request $request)
    {
        $user = $request->user();
        $data = $this->usersTransformer->transform($user);
        return $this->respond(trans('messages.global.success'), $data);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function authenticatePersonalToken(Request $request)
    {
        $this->validate($request, [
            'client_id' => 'required',
            'client_secret' => 'required',
        ]);
        $request->request->add([
            'grant_type' => 'client_credentials',
            'client_id' => $request->client_id,
            'client_secret' => $request->client_secret,
            'scope' => '*',
        ]);
        $proxy = Request::create(
            '/oauth/token',
            'POST'
        );

        return Route::dispatch($proxy);
    }

}
