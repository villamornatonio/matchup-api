<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Repositories\Api\NotificationRepository;
use Illuminate\Http\Request;

class NotificationController extends ApiController
{
    protected $notificationRepository;

    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    public function index(Request $request)
    {
        $data = $this->notificationRepository->allOwned($request);

        return $this->respondPaginated('Success', $data);

    }

    public function markAsRead($id, Request $request)
    {
        $data = $this->notificationRepository->show($id);
        $this->authorize('api.notification.owned', $data);

        $data = $this->notificationRepository->markAsRead($id, $request);

        return $this->respond('Success', $data);

    }

    public function markAllAsRead(Request $request)
    {
        $data = $this->notificationRepository->markAllAsRead($request);

        return $this->respond('Success', $data);
    }
}
