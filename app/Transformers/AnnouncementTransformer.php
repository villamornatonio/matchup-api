<?php

namespace App\Transformers;


/**
 * Class AnnouncementTransformer
 * @package App\Transformers
 */
class AnnouncementTransformer extends Transformer
{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($item)
    {

        return [
            'id' => $item['id'],
            'title' => $item['title'],
            'content' => $item['content'],
            'date' => $item['date'],
            'created_by' => ucfirst($item['created_by']['first_name']) . ' ' . ucfirst($item['created_by']['last_name']),
            'school' => $item['school']['short_name'],
            'is_owned' => ($item['created_by']['id'] === request()->user()->id)
        ];
    }


}

