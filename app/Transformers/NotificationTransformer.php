<?php

namespace App\Transformers;


use Carbon\Carbon;

class NotificationTransformer extends Transformer
{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($item)
    {

        return [
            'id' => $item['id'],
            'title' => $item['title'],
            'content' => $item['content'],
            'icon' => $item['icon'],
            'is_read' => $item['is_read'],
            'created_at' => Carbon::parse($item['created_at'])->toFormattedDateString(),
        ];
    }


}

