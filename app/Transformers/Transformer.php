<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

/**
 * Class Transformer
 * @package App\Transformers
 */
abstract class Transformer extends TransformerAbstract
{


    /**
     * @param array $items
     * @return array
     */
    public function transformCollection(array $items)
    {

        return array_map([$this, 'transform'], $items);

    }

    /**
     * @param $item
     * @return mixed
     */
    public abstract function transform($item);

}