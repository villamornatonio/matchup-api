<?php

namespace App\Console\Commands;

use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Delete7DaysOldUserNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:deleteOldNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete 7 Days Old User Notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         Notification::where('created_at', '<', Carbon::now()->subDays(7))->delete();
    }
}
