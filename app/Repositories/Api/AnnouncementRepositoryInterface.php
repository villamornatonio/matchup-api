<?php namespace App\Repositories\Api;

/**
 * Interface RepositoryInterface
 * @package App\Repositories
 */
/**
 * Interface AnnouncementRepositoryInterface
 * @package App\Repositories\Api
 */
interface AnnouncementRepositoryInterface
{

    /**
     * @return mixed
     */
    public function all();

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $id
     * @return mixed
     */
    public function show($id);

    /**
     * @param $request
     * @return mixed
     */
    public function ownSchool($request);

    /**
     * @param $request
     * @return mixed
     */
    public function deleteAllOwned($request);
}