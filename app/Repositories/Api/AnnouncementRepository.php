<?php namespace App\Repositories\Api;

use App\Models\Announcement;
use App\Repositories\RepositoryAbstract;
use App\Transformers\AnnouncementTransformer;

/**
 * Class AnnouncementRepository
 * @package App\Repositories\Api
 */
class AnnouncementRepository extends RepositoryAbstract implements AnnouncementRepositoryInterface
{

    /**
     * @var Announcement
     */
    protected $model;
    /**
     * @var AnnouncementTransformer
     */
    protected $transformer;

    /**
     * AnnouncementRepository constructor.
     * @param Announcement $announcement
     * @param AnnouncementTransformer $announcementTransformer
     */
    function __construct(Announcement $announcement, AnnouncementTransformer $announcementTransformer)
    {
        $this->model = $announcement;
        $this->transformer = $announcementTransformer;
    }


    /**
     * @return $this
     */
    public function all()
    {
        $this->data = $this->model->all()->toArray();

        return $this;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);

    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id)
    {
        return $this->model->find($id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();

    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {

        $this->data = $this->model->find($id);

        return $this->data;
    }

    /**
     * @param $request
     * @return $this
     */
    public function ownSchool($request)
    {
        $model= $this->model->where('school_id', $request->user()->school_id);


//        $model = $this->model->where('user_id', $request->user()->id);


        $data = $model->paginate(5)->toArray();

        $transformedData = $this->transformer->transformCollection($data['data']);

        $data['data'] = $transformedData;

        return $data;

//        return $this;

    }

    /**
     * @param $request
     * @return mixed
     */
    public function deleteAllOwned($request)
    {
        return $this->model->where('created_by', $request->user()->id)->delete();
    }

}