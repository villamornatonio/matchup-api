<?php namespace App\Repositories\Api;

use App\Repositories\RepositoryAbstract;
use App\Repositories\RepositoryInterface;
use App\Transformers\UsersTransformer;
use App\User;

/**
 * Class UserRepository
 * @package App\Repositories\Api
 */
class UserRepository extends RepositoryAbstract implements RepositoryInterface
{
    /**
     * @var User
     */
    protected $model;

    /**
     * UserRepository constructor.
     * @param User $user
     * @param UsersTransformer $usersTransformer
     */
    public function __construct(User $user, UsersTransformer $usersTransformer)
    {
        $this->model = $user;
        $this->transformer = $usersTransformer;
    }

    /**
     * @return $this
     */
    public function all()
    {
        $this->data = $this->model->all()->toArray();

        return $this;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);

    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id)
    {
        return $this->model->find($id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->find($id)->destroy();

    }

    /**
     * @param $id
     * @return $this
     */
    public function show($id)
    {
        $this->data = $this->model->find($id);

        return $this;
    }

    public function paginatedStudents($request)
    {

        $query = $this->model->query();
        $data = $query->paginate($request->per_page)->toArray();

        $transformedData = $this->transformer->transformCollection($data['data']);
//        unset($rawData['data']);
        $data['data'] = $transformedData;
        $data['message'] = 'Success';
        $data['code'] = 200;

        return $data;

    }

}