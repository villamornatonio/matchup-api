<?php namespace App\Repositories\Api;

use App\Http\Resources\StudentResource;
use App\Repositories\RepositoryAbstract;
use App\Transformers\StudentTransformer;
use App\User;
use Illuminate\Support\Facades\DB;


/**
 * Class StudentRepository
 * @package App\Repositories\Api
 */
class StudentRepository extends RepositoryAbstract implements StudentRepositoryInterface
{

    /**
     * @var User
     */
    protected $model;


    /**
     * StudentRepository constructor.
     * @param User $user
     * @param StudentTransformer $studentTransformer
     */
    public function __construct(User $user, StudentTransformer $studentTransformer)
    {
        $this->model = $user;
        $this->transformer = $studentTransformer;
    }


    /**
     * @param $request
     * @return mixed
     */
    public function paginatedStudents($request)
    {
        $model = $this->model->whereHas('role.role',function($query) use ($request){
            $query->where('name','student');
        })->whereHas('userProfile',function($query) use ($request){
            $query->where('school_id',$request->user()->userProfile->school_id);
        });

        if ($request->has('query') && $request->get('query') != '') {
            $model = $model->where(function ($query) use ($request) {
                $query->where('first_name', 'like', '%' . $request->get('query') . '%');
                $query->OrWhere('last_name', 'like', '%' . $request->get('query') . '%');
                $query->OrWhere('email', 'like', '%' . $request->get('query') . '%');
                $query->OrWhere(DB::raw('CONCAT(first_name, " ", last_name)'), 'like',
                    '%' . $request->get('query') . '%');
            });
        }

        $data = $model->paginate($request->per_page)->toArray();

        $transformedData = $this->transformer->transformCollection($data['data']);

        $data['data'] = $transformedData;

        return $data;

    }

}