<?php namespace App\Repositories\Api;

interface NotificationRepositoryInterface
{

    /**
     * @return mixed
     */
    public function all();

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    public function show($id);

    public function allOwned($request);

    public function markAsRead($id, $request);

    public function markAllAsRead($request);


}