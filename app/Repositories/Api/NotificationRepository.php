<?php namespace App\Repositories\Api;

use App\Models\Announcement;
use App\Models\Notification;
use App\Repositories\RepositoryAbstract;
use App\Transformers\AnnouncementTransformer;
use App\Transformers\NotificationTransformer;

/**
 * Class AnnouncementRepository
 * @package App\Repositories\Api
 */
class NotificationRepository extends RepositoryAbstract implements NotificationRepositoryInterface
{

    /**
     * @var Announcement
     */
    protected $model;
    /**
     * @var AnnouncementTransformer
     */
    protected $transformer;

    /**
     * AnnouncementRepository constructor.
     * @param Announcement|Notification $announcement
     * @param NotificationTransformer $notificationTransformer
     * @internal param AnnouncementTransformer $announcementTransformer
     */
    function __construct(Notification $notification, NotificationTransformer $notificationTransformer)
    {
        $this->model = $notification;
        $this->transformer = $notificationTransformer;
    }


    /**
     * @return $this
     */
    public function all()
    {
        $this->data = $this->model->all()->toArray();

        return $this;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);

    }

    public function show($id)
    {

        $this->data = $this->model->find($id);

        return $this->data;
    }


    public function allOwned($request)
    {
        $model = $this->model->where('user_id', $request->user()->id);


        $data = $model->paginate(5)->toArray();

        $transformedData = $this->transformer->transformCollection($data['data']);

        $data['data'] = $transformedData;

        return $data;


    }

    public function markAsRead($id, $request)
    {
        return $this->model->where('id', $id)->where('user_id', $request->user()->id)->update(['is_read' => 1]);
    }

    public function markAllAsRead($request)
    {
        return $this->model->where('user_id', $request->user()->id)->update(['is_read' => 1]);
    }


}