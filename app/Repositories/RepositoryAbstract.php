<?php namespace App\Repositories;

/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 17/11/2018
 * Time: 10:35 AM
 */
/**
 * Class RepositoryAbstract
 * @package App\Repositories
 */
/**
 * Class RepositoryAbstract
 * @package App\Repositories
 */
abstract class RepositoryAbstract
{
    /**
     * @var
     */
    protected $transformer;
    /**
     * @var
     */
    protected $data;

    /**
     * @return array
     */
    public function transformed()
    {
        $response = [];;
        if (count($this->data) == 1) {
            $response = $this->transformer->transform($this->data[0]);
        } else {
            $response = $this->transformer->transformCollection($this->data);
        }

        return $response;

    }

    /**
     * @return array
     */
    public function transformedCollection()
    {
        $response = [];
        $response = $this->transformer->transformCollection($this->data);

        return $response;
    }

    public function transformedSingle()
    {
        $response = [];
        $response = $this->transformer->transform($this->data);

        return $response;
    }

    public function get()
    {
        $response = $this->data;

        return $response;
    }
}