<?php

namespace App;

use App\Models\Role;
use App\Models\School;
use App\Models\UserProfile;
use App\Models\UserRole;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'school_id',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('defaultsRelationships', function ($builder) {
            $builder->with(['role.role', 'userProfile']);
        });

    }

    public function role()
    {
        return $this->hasOne(UserRole::class);
    }

    public function userProfile()
    {
        return $this->hasOne(UserProfile::class);
    }




}
