<?php

namespace App\Providers;

use App\Models\Announcement;
use App\Policies\AnnouncementPolicy;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin.teachers.index', 'App\Policies\Api\Admin\TeacherPolicy@index');
        Gate::define('api.superadmin.only', 'App\Policies\ApiSuperAdminOnlyPolicy@only');
        Gate::define('api.admin.only', 'App\Policies\ApiAdminOnlyPolicy@only');
        Gate::define('api.teacher.only', 'App\Policies\ApiTeacherOnlyPolicy@only');
        Gate::define('api.guardian.student.only', 'App\Policies\ApiGuardianAndStudentOnlyPolicy@only');
        Gate::define('api.announcement.owned', 'App\Policies\AnnouncementPolicy@owned');
        Gate::define('api.notification.owned', 'App\Policies\NotificationPolicy@owned');

        Passport::routes();

        Passport::tokensExpireIn(Carbon::now()->addDays(15));

        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
    }
}
