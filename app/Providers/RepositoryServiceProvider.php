<?php

namespace App\Providers;

use App\Repositories\Api\AnnouncementRepository;
use App\Repositories\Api\AnnouncementRepositoryInterface;
use App\Repositories\Api\StudentRepository;
use App\Repositories\Api\StudentRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AnnouncementRepositoryInterface::class, AnnouncementRepository::class);
        $this->app->bind(StudentRepositoryInterface::class, StudentRepository::class);
    }
}
