<?php

namespace App\Providers;

use App\Repositories\Api\AnnouncementRepository;
use App\Repositories\Api\AnnouncementRepositoryInterface;
use App\Repositories\Api\Users\UserRepositoryInterface;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Support\ServiceProvider;

class DatabaseProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind(UserRepositoryInterface::class, EloquentUserProvider::class);
    }
}
