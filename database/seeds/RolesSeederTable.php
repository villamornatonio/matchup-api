<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['name' => 'superadmin','label' => 'Super Administrator', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'admin','label' => 'School Administrator', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'teacher','label' => 'School Teacher', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'guardian','label' => 'Guardian', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'student','label' => 'Student', 'created_at' => new DateTime, 'updated_at' => new DateTime],
        ];
        DB::table('roles')->insert($roles);
    }
}
