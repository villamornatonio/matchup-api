<?php

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $roleSuperAdmin = Role::where('name', 'superadmin')->first();
        $roleAdmin = Role::where('name', 'admin')->first();
        $roleTeacher = Role::where('name', 'teacher')->first();
        $roleGurdian = Role::where('name', 'guardian')->first();
        $roleStudent = Role::where('name', 'student')->first();


        $superAdmin = new \App\User();
        $superAdmin->first_name =  'Super Admin';
        $superAdmin->last_name = 'Super Admin';
        $superAdmin->email = 'superadmin@skuyla.com';
        $superAdmin->password = Hash::make('password');
        $superAdmin->school_id = 2;
        $superAdmin->save();

        $superAdminUserRole = new \App\Models\UserRole();
        $superAdminUserRole->role_id = $roleSuperAdmin->id;
        $superAdminUserRole->user_id = $superAdmin->id;
        $superAdminUserRole->save();


        $admin = new \App\User();
        $admin->first_name =  'Admin';
        $admin->last_name = 'Admin';
        $admin->email = 'admin@skuyla.com';
        $admin->password = Hash::make('password');
        $admin->school_id = 2;
        $admin->save();

        $adminUserRole = new \App\Models\UserRole();
        $adminUserRole->role_id = $roleAdmin->id;
        $adminUserRole->user_id = $admin->id;
        $adminUserRole->save();

        $adminUserProfile = new \App\Models\UserProfile();
        $adminUserProfile->user_id = $admin->id;
        $adminUserProfile->school_id = 2;
        $adminUserProfile->rfid = $admin->id;
        $adminUserProfile->avatar = 'default-user.png';
        $adminUserProfile->save();


        $teacher = new \App\User();
        $teacher->first_name =  'Teacher';
        $teacher->last_name = 'Teacher';
        $teacher->email = 'teacher@skuyla.com';
        $teacher->password = Hash::make('password');
        $teacher->school_id = 2;
        $teacher->save();

        $teacherUserRole = new \App\Models\UserRole();
        $teacherUserRole->role_id = $roleTeacher->id;
        $teacherUserRole->user_id = $teacher->id;
        $teacherUserRole->save();

        $teacherUserProfile = new \App\Models\UserProfile();
        $teacherUserProfile->user_id = $teacher->id;
        $teacherUserProfile->school_id = 2;
        $teacherUserProfile->rfid = $teacher->id;
        $teacherUserProfile->avatar = 'default-user.png';
        $teacherUserProfile->save();

        $guardian = new \App\User();
        $guardian->first_name =  'Guardian';
        $guardian->last_name = 'Guardian';
        $guardian->email = 'guardian@skuyla.com';
        $guardian->password = Hash::make('password');
        $guardian->school_id = 2;
        $guardian->save();

        $guardianUserRole = new \App\Models\UserRole();
        $guardianUserRole->role_id = $roleGurdian->id;
        $guardianUserRole->user_id = $guardian->id;
        $guardianUserRole->save();

        $guardianUserProfile = new \App\Models\UserProfile();
        $guardianUserProfile->user_id = $guardian->id;
        $guardianUserProfile->school_id = 2;
        $guardianUserProfile->rfid = $guardian->id;
        $guardianUserProfile->avatar = 'default-user.png';
        $guardianUserProfile->save();

        $student = new \App\User();
        $student->first_name =  'Student';
        $student->last_name = 'Student';
        $student->email = 'student@skuyla.com';
        $student->password = Hash::make('password');
        $student->school_id = 2;
        $student->save();

        $studentUserRole = new \App\Models\UserRole();
        $studentUserRole->role_id = $roleStudent->id;
        $studentUserRole->user_id = $student->id;
        $studentUserRole->save();

        $studentUserProfile = new \App\Models\UserProfile();
        $studentUserProfile->user_id = $student->id;
        $studentUserProfile->school_id = 2;
        $studentUserProfile->rfid = $student->id;
        $studentUserProfile->avatar = 'default-user.png';
        $studentUserProfile->save();


    }
}
