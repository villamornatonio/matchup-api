<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schools = [
            ['short_name' => 'NOSCHOOL','long_name' => 'NO SCHOOL','address' => 'No Address','avatar_url' => 'no-school.png', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['short_name' => 'FCRA','long_name' => 'FIRST CHINESE ACADEMY','address' => 'San Isidro','avatar_url' => 'fcra.png', 'created_at' => new DateTime, 'updated_at' => new DateTime],
        ];
        DB::table('schools')->insert($schools);
    }
}
