<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Faker::create();
        foreach (range(1, 5000) as $index) {
            $student = new \App\User();
            $student->first_name =  $fake->firstName();
            $student->last_name = $fake->lastName();
            $student->email = $fake->unique()->email;
            $student->password = Hash::make('password');
            $student->school_id = 2;
            $student->save();

            $studentUserRole = new \App\Models\UserRole();
            $studentUserRole->role_id = 5;
            $studentUserRole->user_id = $student->id;
            $studentUserRole->save();

            $studentUserProfile = new \App\Models\UserProfile();
            $studentUserProfile->user_id = $student->id;
            $studentUserProfile->school_id = 2;
            $studentUserProfile->rfid = $student->id;
            $studentUserProfile->avatar = 'default-user.png';
            $studentUserProfile->save();
        }

    }
}
